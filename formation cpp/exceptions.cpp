#include <iostream>
#include <stdexcept>

double calcule_prix(int duree) noexcept {
    return 1000 + 299.90*duree;
}

void afficher_namibie() noexcept {
    std::cout << "* Namibie :" << std::endl;
    int duree;
    std::cout << "Duree ? ";
    std::cin >> duree;

    try {
        if(duree == 0)
            throw 0;
        if(duree < 0)
        // https://en.cppreference.com/w/cpp/error
            throw std::range_error("La duree doit etre superieure a 0");
        double prix = calcule_prix(duree);
        double prix_journalier = prix/duree;
        std::cout << "Prix par jour : " << prix_journalier << std::endl;
    } catch(int& exc) {
        if(exc == 0)
            std::cerr << "Exception 0" << std::endl;
        else
            std::cerr << "Exception numerique : " << exc << std::endl;
    } catch(std::range_error& exc) {
        std::cerr << "Exception d'intervale : " << exc.what() << std::endl;
    } catch(...) {
        std::cerr << "Exception inattendue" << std::endl;
    }
    std::cout << "Fin de la Namibie" << std::endl;
}
