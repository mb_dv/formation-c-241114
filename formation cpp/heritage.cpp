#include <iostream>
#include "heritage.h"

DestinationMaritime::DestinationMaritime():Destination(), ile("") {}

DestinationMaritime::DestinationMaritime(std::string nom, std::string pays,
            unsigned int jours, std::string ile):
    Destination(nom, pays, jours), ile(ile) {}

void DestinationMaritime::setIle(std::string ile) {
    this->ile = ile;
}

void DestinationMaritime::afficher() {
    Destination::afficher();
    std::cout << "  (" << ile << ")" << std::endl;
}

void afficher_italie() {
    std::cout << "* Italie :" << std::endl;

    DestinationMaritime dm1;
    dm1.setNom("Palerme");
    dm1.setPays("Italie");
    dm1.setJours(3);
    dm1.setIle("Sicile");
    dm1.afficher();

    DestinationMaritime dm2("Catane", "Italie", 1, "Sicile");
    dm2.afficher();
    DestinationMaritime dm3("Siracuse", "Italie", 2, "Sicile");
    Destination d1("Gene", "Italie", 2);

    std::cout << " * Tout : " << std::endl;
    //DestinationMaritime* dms[] = { &dm1, &dm2, &dm3 };
    Destination* dms[] = { &dm1, &dm2, &dm3, &d1 };
    for(size_t i = 0; i < 4 ; i++)
        dms[i]->afficher();
}
