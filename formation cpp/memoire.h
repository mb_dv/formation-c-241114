#include <iostream>

#ifndef MEMOIRE_H_INCLUDED
#define MEMOIRE_H_INCLUDED

void afficher_grece();

class Voyage {
private:
    std::string nom;
    double prix;
public:
    Voyage();
    Voyage(std::string nom, double prix);
    Voyage(const Voyage& autre);
    virtual ~Voyage() noexcept;
    void afficher();
    bool operator==(const Voyage& autre);
    bool operator!=(const Voyage& autre);
    Voyage& operator=(const Voyage& autre);
    // virtual void imprimer() = 0;
};

#endif // MEMOIRE_H_INCLUDED
