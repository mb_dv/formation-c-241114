#include "objets.h"

#ifndef HERITAGE_H_INCLUDED
#define HERITAGE_H_INCLUDED

void afficher_italie();

class DestinationMaritime : public Destination {
private:
    std::string ile;
public:
    DestinationMaritime();
    DestinationMaritime(std::string nom, std::string pays,
            unsigned int jours, std::string ile);
    void afficher();
    void setIle(std::string ile);

};

#endif // HERITAGE_H_INCLUDED
