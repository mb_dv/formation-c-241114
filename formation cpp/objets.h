#ifndef OBJETS_H_INCLUDED
#define OBJETS_H_INCLUDED

class Destination {
private:
    std::string nom;
    std::string pays;
    unsigned int jours;
public:
    Destination();
    Destination(std::string nom);
    Destination(std::string nom, std::string pays, unsigned int jours);
    virtual ~Destination();
    virtual void afficher();
    virtual void allonger();
    virtual void allonger(int j);
    virtual void raccourcir(int j);
    void setNom(std::string nom);
    void setPays(std::string pays);
    void setJours(int jours);
};

void afficher_espagne();

#endif // OBJETS_H_INCLUDED
