#include <iostream>

//template<class T> T calcule_eau(T km) {
template<typename T> T calcule_eau(T km) {
    return 0.333+km*0.25;
}
template<> int calcule_eau(int km) {
    return 1+km/4;
}

void afficher_egypte() {
    std::cout << "* Egypte : " << std::endl;
    unsigned int km_i = 13;
    std::cout << calcule_eau(km_i) << "l d'eau" << std::endl; // 3
    float km_f = 13.4f;
    std::cout << calcule_eau(km_f) << "l d'eau" << std::endl; // 3.6
    std::cout << calcule_eau( int(km_f)+1.0 ) << "l d'eau" << std::endl; // 3.8
    std::cout << calcule_eau<int>( int(km_f)+1.0 ) << "l d'eau" << std::endl; // 4

    // NON std::cout << calcule_eau("treize") << "l d'eau" << std::endl; // ???
    // C++11 :
    std::string* x = nullptr;
    // double km_d = 12.89;
    // double km_d (12.89);
    double km_d { 12.89 };
    auto km_ll = 100000000000;
    auto eau_ll = calcule_eau(km_ll);
}
