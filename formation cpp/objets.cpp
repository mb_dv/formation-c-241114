#include <iostream>
#include "objets.h"

// avant C++11 Destination::Destination():nom(""),pays(""),jours(1) {}
Destination::Destination():Destination("") {}

Destination::Destination(std::string nom):Destination(nom, "", 1) {}

Destination::Destination(std::string nom, std::string pays, unsigned int jours):
    nom(nom),pays(pays),jours(1) { setJours(jours); }

Destination::~Destination() { std::cout << nom << " : del" << std::endl; }

void Destination::afficher() {
    std::cout << nom << " (" << pays << ") : "
        << jours << " jours" << std::endl;
}

void Destination::allonger() {
    allonger(1);
}

// avant : this = &d1;
void Destination::allonger(int jours) {
    //(*this).jours += jours;
    // this->jours += jours;
    setJours(this->jours + jours);
}

void Destination::raccourcir(int j) {
    allonger(-j);
}

void Destination::setNom(std::string nom) {
    this->nom = nom;
}
void Destination::setPays(std::string pays) {
    this->pays = pays;
}
void Destination::setJours(int jours) {
    if(jours < 1)
        return ;
    this->jours = jours;
}

void afficher_espagne() {
    std::cout << "* Espagne" << std::endl;
    {
        Destination d1; // surtout pas de ()
        // d1.nom = "Madrid"; d1.pays = "Espagne"; d1.jours = 4;
        d1.setNom("Madrid");
        d1.setPays("Espagne");
        d1.setJours(4);
        d1.afficher();
        d1.allonger(6);
        d1.raccourcir(10);
        d1.afficher(); // ... 7 jours
    }

    Destination d2("Alhambra", "Espagne", 2);
    d2.afficher();
}
