#include <iostream>
#include "memoire.h"

void ajouter_tva_val(float prix) {
    prix = prix * 1.2f;
}
void ajouter_tva_ptr(float * prix) {
    *prix = *prix * 1.2f;
}
void ajouter_tva_ref(float& prix) {
    prix = prix * 1.2f;
}

void afficher_prix_grece() {
    float prix = 1090.99f;
    float * pprix = &prix;
    float & rprix = prix;
    float prix2 = prix;
    prix = 980.20f;

    std::cout << "Prix : " << prix << std::endl; // 980
    std::cout << "Prix (ptr) : " << *pprix << std::endl;  // 980
    std::cout << "Prix (ref) : " << rprix << std::endl; // 980
    std::cout << "Prix (copie) : " << prix2 << std::endl;  // 1090.99
    // impossible avec une reference :
    pprix = pprix + 10;
    pprix = NULL;

    prix = 980;
    ajouter_tva_val(prix);
    std::cout << "Prix (TVA 1) " << prix << std::endl; // 980

    prix = 980;
    ajouter_tva_ptr(&prix);
    std::cout << "Prix (TVA 1) " << prix << std::endl; // 1176

    prix = 980;
    ajouter_tva_ref(prix);
    std::cout << "Prix (TVA 1) " << prix << std::endl; // 1176
    // NON ajouter_tva_ref(10);
    // ajouter_tva_ref(prix+10);
}

double* lire_couts_grece() {
    double* couts = new double[3];
    couts[0] = 679.12;
    couts[1] = 390.22;
    couts[2] = 45.90;
    return couts;
}

void afficher_couts_grece() {
    double* couts = lire_couts_grece();
    double tot = 0;
    for(size_t i = 0; i < 3 ; i ++)
        tot += couts[i];
    std::cout << "Somme des couts : " << tot << std::endl;
    delete couts;
}

// Voyage::Voyage():nom(""), prix(0) {}
Voyage::Voyage():Voyage("", 0) {}
// Voyage::Voyage() {}
Voyage::Voyage(std::string nom, double prix):nom(nom), prix(prix) {}
Voyage::Voyage(const Voyage& autre):
        nom(autre.nom),prix(autre.prix) {}
Voyage::~Voyage() noexcept {}
void Voyage::afficher() {
    std::cout << nom << " : " << prix << "e" << std::endl;
}
bool Voyage::operator==(const Voyage& autre) {
    return this->nom == autre.nom;
}
bool Voyage::operator!=(const Voyage& autre) {
    return this->nom != autre.nom;
}
Voyage& Voyage::operator=(const Voyage& autre) {
    nom = autre.nom;
    prix = autre.prix;
    return *this;
}

void afficher_offre_grece() {
    Voyage v1("Fantastique Pyree", 1239.99);
    v1.afficher();
    Voyage * v2 = new Voyage("Super Moree", 1390.00);
    v2->afficher();
    delete v2;

    Voyage v3;
    v3 = v1;
    v3.afficher();
    std::cout << (&v1 == &v3) << std::endl; // 0
    std::cout << (v1 == v3) << std::endl; // 1
    std::cout << (v1 == *v2) << std::endl; // 0
    std::cout << (v1 != *v2) << std::endl; // 1
    Voyage v4(v3);
    v4.afficher();
    Voyage v5 = v1; // Voyage v5(v1);
}

void afficher_grece() {
    std::cout << "* Grece : " << std::endl;
    // afficher_prix_grece();
    // afficher_couts_grece();
    afficher_offre_grece();
}
