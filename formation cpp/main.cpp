#include <iostream>
#include "objets.h"
#include "heritage.h"
#include "memoire.h"
#include "exceptions.h"
#include "templates.h"
#include "bibliotheques.h"

namespace DV {

    namespace Mann {
        void afficher() {
            std::cout << "(dont ile de Mann)" << std::endl;
        }
    }

    namespace Irlande {

        void afficher() {
            std::cout << "* Irlande" << std::endl;
            Mann::afficher();
            std::cout << "Voyageurs ? ";
            unsigned voyageurs;
            std::cin >> voyageurs;
            std::cout << "Prix : " << (863.90+630*voyageurs) <<
                " euros" << std::endl;
        }
    }
}

int main()
{
    std::cout << "Bienvenue a l'appli " << 2 << std::endl;
    // DV::Irlande::afficher();
    // afficher_espagne();
    // afficher_italie();
    // afficher_grece();
    // afficher_namibie();
    // afficher_egypte();
    afficher_inde();
    return 0;
}
