#include <iostream>
#include <string>
#include <cstdio>
#include <regex>
#include <vector>
#include <set>
#include <algorithm>
#include <map>
#include "bibliotheques.h"


// pdf pr�sence : xdv@askywhale.com
// evaluation : https://forms.office.com/e/VMRDM2SKQd


void afficher_devis() {
    std::string nom, email, interro1;
    interro1 = "Nom du client ? ";
    std::cout << interro1;
    std::cin >> nom;
    std::cout << "Email ?";
    std::cin >> email;
    std::cout << "Devis pour " << nom.c_str() << " : " << std::endl;

    double prix = 1340.90;
    char prix_char[100];
    snprintf(prix_char, 100, "Prix : %.2f euros", prix);
    std::string prix_str(prix_char);
    if(prix>1200)
        // prix_str = prix_str + " !";
        prix_str += " !";
    std::cout << prix_str << std::endl;

    if (nom.size()<2)
        std::cout << "Nom trop court" << std::endl;
    if (nom=="Carl")
        std::cout << "Pas de voyage pour Carl" << std::endl;
    if (nom[0]=='0')
        std::cout << "Nom etrange" << std::endl;
    if (nom.find("zc") != std::string::npos )
        std::cout <<
            std::string("Bienvenue aux polonais dans notre agence").substr(0,9)
            << std::endl;
    // = 1, * : 0ou+,  + : 1ou+, ? : 0ou1, {3} : 3, {3,5} : 3�5
    std::regex que_des_lettres("[A-Z][a-z\\-]+"); // , std::regex_constants::icase
    // if( ! std::regex_search(nom, que_des_lettres))
    if( ! std::regex_match(nom, que_des_lettres))
        std::cout << "Nom incorrect" << std::endl;
    std::regex regex_telephone("\\+?[0-9]{10,14}");
    std::regex regex_code_postal("[0-9ab]{5}");
    std::regex regex_email("[a-z\\.\\-0-9]+@[a-z\\.\\-0-9]+");
    if( ! std::regex_match(email, regex_email))
        std::cout << "Adresse email incorrecte" << std::endl;
}

void preparer_programme() {
    bool sortir = false;
    // set : x => valeur
    // []/vector/list/array : index (0...) => valeur
    // map : valeur1 => valeur2
    std::vector<std::string> etapes;
    while(!sortir) {
        std::cout << "Etape suivante (\"q\" : quitter) ? ";
        std::string etape;
        std::cin >> etape;
//        sortir = (etape=="q");
        if(etape=="q")
            sortir = true;
        else
            etapes.push_back(etape);
    }

    size_t nb_etapes = etapes.size();
    for(size_t i = 0 ; i < nb_etapes ; i ++) {
        std::cout << (i+1) << " - " << etapes[i] << std::endl;
    }
    /* for(std::vector<std:string>::iterator it = etapes.begin() ;
            it != etapes.end() ; it++) {
        std::cout << *it << std::endl;
    } */
    size_t nb_randos = 0;
    for(const std::string& etape : etapes) // (C++11 )
        // if (etape == "rando")
        if (etape.find("rando") != std::string::npos)
            nb_randos ++;
            //std::cout << etape << std::endl;
    std::cout << "En tout, " << nb_randos << " randos" << std::endl;

    std::map<std::string,int> gains {
        {"rando",-1700}, {"visites",-450}, {"repos",380}
    };

    if (gains.find("visites") != gains.end())
        std::cout << "Visites : " << gains["visites"] << " calories" << std::endl;
    int bilan = 0;
    /*for(const std::string& etape : etapes) {
        if (gains.find(etape) != gains.end())
            bilan += gains[etape];
    }*/
    for(auto paire : gains) {
        int nb = 0;
        for(const std::string& et2 : etapes) {
            if(paire.first == et2)
                bilan += paire.second;
        }
    }
    std::cout << "Bilan calorique : " << bilan << std::endl;
}

void choisir_hotel() {
    std::set<std::string> hotel1 {"piscine", "jacuzzi", "bar" };
    std::set<std::string> hotel2 {"piscine", "fitness" };
    if (hotel1.find("piscine") != hotel1.end())
        std::cout << "Hotel 1 a une piscine " << std::endl;

    std::set<std::string> tous_hotel = hotel1;
    tous_hotel.insert(hotel2.begin(), hotel2.end());
    if(tous_hotel.find("fitness") != tous_hotel.end() )
        std::cout << "Il y a du fitness en Inde " << std::endl;

}

void afficher_villes_inde() {
    std::vector<std::string> villes {"Bangalore", "Delhi", "Calcutta"};

    std::sort(villes.begin(), villes.end());
    for(const std::string& v : villes) {
        std::cout << v << std::endl;
    }

    struct StrLongueurComparateur {
        bool operator()(std::string a, std::string b) {
            return a.size() < b.size();
        }
    };
    StrLongueurComparateur comparateur;
    std::sort(villes.begin(), villes.end(), comparateur);
    for(const std::string& v : villes) {
        std::cout << v << std::endl;
    }

    // C++11 : lamdba
    std::sort(villes.begin(), villes.end(),
        [](std::string a, std::string b) {
            return a.size() < b.size();
        });
    for(const std::string& v : villes) {
        std::cout << v << std::endl;
    }
}

void afficher_inde() {
    std::cout << "* Inde :" << std::endl;
    // afficher_devis();
    // preparer_programme();
    // choisir_hotel();
    afficher_villes_inde();
}
