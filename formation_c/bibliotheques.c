#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

// https://en.cppreference.com/w/c/numeric/math
void afficher_prix_argentine() {
    int jours = 12;
    long prix = lround(870 + log(jours) * 159);
    printf("Prix : %d\n", prix);
}

float* lire_couts_argentine() {
    float* couts = malloc( sizeof(float)*2 );
    couts[0] = 524;
    couts[1] = 543.90f;
    couts = realloc(couts, sizeof(float)*4 );
    couts[2] = 12.10f;
    couts[3] = 0;
    return couts;
}

void afficher_couts_argentine() {
    float* couts = lire_couts_argentine();
    for(size_t i = 0 ; couts[i]!=0 ; i++)
        printf("- %.2f\n", couts[i]);
    free(couts);
}

void afficher_inscription_argentine() {
    printf("Nom ?");
    char nom[100];
    scanf("%s", nom);

    char tel[100];
    do {
        printf("Tel ?");
        scanf("%s", tel);
    } while(strlen(tel)>=20);
    char tel_court[20];
    strcpy(tel_court, tel);

    char nomtel[strlen(nom)+1+strlen(tel_court)+1];
    strcpy(nomtel, nom);
    strcat(nomtel, ",");
    strcat(nomtel, tel_court);
    if(strstr(nom, "Carl")!=NULL)
        printf("Attention, Carl ne doit plus voyager avec nous\n");
    for(int i = 0 ; i < strlen(tel_court) ; i++)
        if(!isdigit(tel_court[i]) && tel_court[i]!='.')
            printf("Tel incorrect\n");

    printf("Inscription de %s\n", nomtel);

     // r,w,a : read,write,ajout ; b : binaire
    FILE* finscription = fopen("bibliotheque.csv", "a");
    fprintf(finscription, nomtel);
    fprintf(finscription, "\n");
    fclose(finscription);

}

void afficher_depart_argentine() {
    time_t maintenant = time(NULL);
    time_t depart = maintenant + 4*7*24*3600;
    printf("Maintenant : %d\n", maintenant);
    printf("Depart : %d\n", depart);

    struct tm* depart_tm = localtime(&depart);
    printf("Depart : %d/%d/%d\n", depart_tm->tm_mday,
        1+depart_tm->tm_mon, 1900+depart_tm->tm_year);
}

void afficher_argentine() {
    printf("* Argentine :\n");
    char* user = getenv("USERNAME");
    printf("Bienvenue %s\n ", user);
    afficher_prix_argentine();
    afficher_couts_argentine();
    afficher_inscription_argentine();
    afficher_depart_argentine();
}
