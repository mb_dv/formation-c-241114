#include <stdio.h>
#include "pointeurs.h"

extern int avion;

void afficher_prix_mexique() {
    float prix = 980.99f;
    float* pprix;
    printf("Taille de pprix : %d octets\n", sizeof(pprix)); //8 octets car appli 64bits
    pprix = &prix;
    printf("Pointeur : %x\n", pprix); // 61fdb4
    *pprix += 10;
    printf("Prix : %.2f\n", *pprix); // 990.99
    printf("Prix : %.2f\n", prix); // 990.99

    pprix = NULL;
//    *pprix += 10; // Erreur windows 0xC0000005
    pprix = &prix;
    printf("Prix : %.2f\n", *pprix); // 990.99
}

void allonger_mexique(short* dp) {
    *dp += 1; // augmente de 1 la valeur � l'adresse de dp
}

void afficher_duree_mexique() {
    short duree = 10;
    int d2;
    allonger_mexique(&duree);
    allonger_mexique(&duree);
    // allonger_mexique(& (duree+3) ); // pas une "lvalue"
    printf("Duree : %d jours\n", duree); // 12 jours
}

float calculer_promo_10(float prix) { return prix * 0.9f; }
float calculer_promo_20(float prix) { return prix * 0.8f; }

void afficher_promos_mexique() {
    printf("1 = petite promo, 2 = grande promo ");
    int no_promo;
    scanf("%d", &no_promo);
    float (* fonction_promo)(float) = NULL;
    if(no_promo==1)
        fonction_promo = &calculer_promo_10;
    else
        fonction_promo = &calculer_promo_20;

    float p1 = 890.99f;
    p1 = fonction_promo(p1);
    printf("P1 : %.2f\n", p1);

    float p2 = 1089.99f;
    p2 = fonction_promo(p2);
    printf("P2 : %.2f\n", p2);
}

void afficher_detail_mexique() {
    const char prg1[] = "- plages\n";
    printf(prg1);
    printf("Taille : %d\n", sizeof(prg1)); //10
    const char * prg2 = "- fiestas\n";
    printf(prg2);
    printf("Taille : %d\n", sizeof(prg2)); // 8

    char* cp = prg1;
    printf("%c\n", *cp); // "-"
    printf("%c\n", *prg2); // "-"
    printf("%c\n", prg2[0]); // "-"

    printf("%c\n", prg1[4]); // "a"
    printf("%c\n", *(cp+4)); // "a"

    // reafficher la moiti� de la seconde chaine : "- fets" sans utiliser []
    for(char* cp2 = prg2 ; *cp2!=0 ; cp2++)
        if( (long long)cp2 % 2 == 0)
            printf("%c", *cp2);
}

// float* lire_couts() {
//     float couts[] = { 899.99f, 10.0f, 3.99f, 0.49f };
//     return couts;
// }
size_t lire_couts(float* couts) {
   couts[0] = 899.99f;
   couts[1] = 10.0f;
   couts[2] = 3.99f;
   couts[3] = 0.49f;
   return 4;
}

void afficher_cout_mexique(float* couts, size_t taille) {
    float tot = 0;
    for(size_t i = 0 ; i < taille ; i++)
        tot += couts[i];
    printf("\nPrix total : %.2f", tot);
}

void afficher_mexique() {
    printf("* Mexique :\n");
    printf("Avion : %d\n", avion);
    afficher_prix_mexique();
    afficher_duree_mexique();
    afficher_promos_mexique();
    afficher_detail_mexique();
    //float* couts = lire_couts();
    float couts[100];
    size_t nb = lire_couts(couts);
    afficher_cout_mexique(couts, 4);
}


