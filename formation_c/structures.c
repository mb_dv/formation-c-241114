#include <stdio.h>

typedef unsigned int entier_positifs;

typedef struct {
    char* nom;
    unsigned long population;
} ville_bresilienne;

typedef union {
    char* nom;
    char* surnom;
    char* email;
    long long id;
} voyageur;

void afficher_une_ville(ville_bresilienne v) {
    printf("%s %d\n", v.nom, v.population);
}

void afficher_bresil() {
    printf("* Bresil :\n");
    entier_positifs jours = 18;
    printf("Jours : %d\n", jours);
    {
        ville_bresilienne v1;
        v1.nom = "Brasilia";
        v1.population = 2300000;
        // printf("%s %d\n", v1.nom, v1.population);
        afficher_une_ville(v1);

        ville_bresilienne v2 = { "Sao Paolo", 5700000 };
        afficher_une_ville(v2);

        ville_bresilienne v3 = { .population=7200000, .nom="Rio" };
        afficher_une_ville(v3);

        ville_bresilienne* pv3 = &v3;
        // printf("%s\n", (*pv3).nom);
        printf("%s\n", pv3->nom);
    }

    voyageur bob;
    bob.email = "bob@yopmail.fr";
    printf("%s\n", bob.nom); // bob@yopmail.fr
    printf("%d\n", bob.nom == bob.id); // 1
    printf("%d\n", sizeof(bob)); // 8
    bob.id = 123321;
    printf("%d\n", bob.surnom); // 123321
    bob.nom = "Bob";
    printf("%s\n", bob.email); // Bob
}
