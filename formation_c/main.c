#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "pointeurs.h"
#include "structures.h"
#include "bibliotheques.h"

void afficher_geographie(void);
void afficher_programme(int duree);
float calculer_total(int duree, int avion, int hotel, float promo) ;
void afficher_canada(void);

int main() {
    printf("Bienvenue dans l'agence de voyage\n");
    // afficher_canada();
#ifdef _POSIX_
    printf("Ce programme ne fonctionne pas sous Linux");
#endif // _POSIX_
    // afficher_mexique();
    // afficher_bresil();
    afficher_argentine();
    return 0;
}

#define HOTEL 48
int avion = 860;

void afficher_canada(void) {
    // Canada : -20% sur 7j � 48� et 860� d'avion
    // pr�parer 4 variables / constantes
    // calculer le prix final
    float promo = 0.2f;
    int duree;
    int hotel = HOTEL;
    printf("Combien de jours ? ");
    scanf("%d", &duree);
    //float prix_total = (1-promo) * (duree*hotel + avion) ;
    float prix_total = calculer_total(duree, avion, hotel, promo) ;
    printf("Prix total Canada : %.2f euros\n", prix_total);
    // en g�n�ral, Air Canada, sauf les voyages courts ( < 7j ) avec Qu�bec'Air
    // sinon pour 7, 14, 21j... Can'Jet
    switch(duree) {
    case 7:
    case 14:
    case 21:
    case 28: printf("Can'Jet\n"); break;
    default:
        if(duree < 7)
            printf("Quebec Air\n");
        else
            printf("Air Canada\n");
    }
    afficher_programme(duree);
    afficher_geographie();
}

float calculer_total(int duree, int avion, int hotel, float promo) {
    return (1-promo) * (duree*hotel + avion);
}

void afficher_geographie(void) {
    unsigned char pays1[] = {'C', 'a', 'n', 'a', 'd', 'a', 0, 'U', 'S', 'A'};
    unsigned char pays2[7] = "Canada";
    // printf(pays2);
    pays2[0] = 'c';
    printf("%s\n", pays1);

    char villes[][9] = {"Montreal", "Toronto", "Ottawa"};
    printf("Premiere ville : %s\n", villes[1]);
}

void afficher_programme(int duree) {
    // Jour 1 : avion
    // Jour 2 : rando
    // Jour 3 : rando
    // Jour 4 : rando
    // Jour 5 : poutine
    // Jour 6 : rando
    // Jour 7 : rando
    // Jour 8 : rando
    // Jour 9 : poutine
    // Jour 10 : avion
    char etapes[duree+1];
    etapes[0] = 0;
    for(unsigned j = 1 ; j <= duree ; j ++) {
        if(j==1 || j==duree) {
            printf("Jour %d : avion\n", j);
            etapes[j] = 1;
        } else if(j%4==1) {
            printf("Jour %d : poutine\n", j);
            etapes[j] = 2;
        } else {
            printf("Jour %d : rando\n", j);
            etapes[j] = 3;
        }
    }

    /*
    unsigned char nb_rando = 0;
    for(unsigned char j = 1 ; j <= duree ; j ++) {
        if(j==1 || j==duree)
            printf("Jour %d : avion\n", j);
        else if(nb_rando==3) {
            printf("Jour %d : poutine\n", j);
            nb_rando = 0;
        } else {
            printf("Jour %d : rando\n", j);
            nb_rando++;
        }
    }*/
    // for(int i=0, j=0 ; i < 20 ; i+=j, j+=1)
    //    printf("%d %d\n", i, j);
    // voir aussi : Duff's Device

    size_t nb_randos = 0;
    for(unsigned k = duree ; k > 0 ; k --) {
        if(etapes[k] == 3) {
            nb_randos++;
        }
    }
    printf("Nombre de jours de rando : %d\n", nb_randos);

    int calories[] = {0, -100, 2000, -1800};
    int total_calories = 0;
    for(unsigned j = 1 ; j <= duree ; j ++) {
        //if (etapes[j] == 1)
        //    total_calories += calories[1];
        // ...
        total_calories += calories[ etapes[j] ];
    }
    const char txt_bilan[] = "Bilan calorique : %d\n";
    printf(txt_bilan, total_calories);
}




